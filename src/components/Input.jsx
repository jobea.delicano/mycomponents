export default function Input({label, ...props}) {
  let labelClass = "block mt-1 mb-1 text-xs font-bold tracking-wide uppercase";

  let inputClass = "mb-4 w-full px-3 py-2 leading-tight border rounded shadow";

  return (
    <>
      <p>
        <label className={labelClass}>{label.toUpperCase()}</label>
        <input className={inputClass} {...props}></input>
      </p>
      
    </>
  );
}
