import { useState, useRef } from 'react';
import { styles } from '../styles';
import { Button, Stack } from '@mui/joy';
import JoyCard from './JoyCard';

export default function UseRef2 () {
    const fileInput = useRef();

    function handleUpload() {
        fileInput.current.click()
    }
    const definition = [
        'Can be used to access DOM elements',
        'Will not cause component to refresh when updated',
        'Value won\'t reset when component refreshes unlike normal variable',
        'Important: When using JoyUI, ref prop should be added in slotProps prop for input fields'
    ]

    return (
        <JoyCard title="useRef #2" definition={definition}>
            <Stack direction="column" justifyContent="center" spacing={1}>
                <p>Normal {`<input type="file" />`}</p>
                <input type="file"/> 

                <p>vs</p>

                <p>Using useRef pointing to {`<input type="file" />`} : </p>                
                <input type="file" className='hidden' ref={fileInput} /> 
                <Button onClick={handleUpload}> Upload File </Button>
            </Stack>
        </JoyCard>
    )
}