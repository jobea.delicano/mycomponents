import { CssVarsProvider, extendTheme } from '@mui/joy/styles';
import { Button, Box, Stack, FormLabel, Input, FormControl, FormHelperText} from '@mui/joy';
import { Typography } from '@mui/joy';

const theme = extendTheme({
    "colorSchemes": {
      "light": {
        "palette": {
          "primary": {
            "solidBg": "#d33b40",
            "solidHoverBg": "#be353a",
            "solidActiveBg": "#be353a",
            "solidDisabledBg": "#f2c4c6",
            "solidDisabledColor": "var(--joy-palette-primary-solidColor)"
          },
          "neutral": {
            "solidBorder": "var(--joy-palette-neutral-600)",
            "solidColor": "var(--joy-palette-neutral-600)",
            "solidBg": "var(--joy-palette-neutral-50)",
            "solidActiveBg": "var(--joy-palette-neutral-50)",
            "solidHoverBg": "var(--joy-palette-neutral-50)",
            "solidDisabledBg": "var(--joy-palette-neutral-50)",
            "solidDisabledColor": "var(--joy-palette-neutral-400)",
            "solidDisabledBorder": "var(--joy-palette-neutral-300)",
            "solidHoverColor": "var(--joy-palette-neutral-900)",
            "solidHoverBorder": "var(--joy-palette-neutral-900)"
          }
        }
      },
      "dark": {
        "palette": {}
      }
    }
  })


export default function Joy() {
    return(
        <CssVarsProvider theme={theme}>
            <Typography level="h2" margin={1}>Joy UI Custom Design Kit for User Manager</Typography>
            <Stack
                margin={1}
                direction="row"
                spacing={2}
            >
                <Stack
                  direction="column"
                  justifyContent="flex-start"
                  alignItems="flex-start"
                  spacing={2}
                >
                    <Button fullWidth>Primary</Button>
                    <Button fullWidth disabled>Disabled</Button>
                    <Button fullWidth loading  loadingPosition="start">Primary</Button>
                </Stack>
                <Stack
                    direction="column"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    spacing={2}
                >
                    <Button fullWidth color="neutral">Secondary</Button>
                    <Button fullWidth color="neutral" disabled >Disabled</Button>
                    <Button fullWidth color="neutral" loading loadingPosition="start" >Loading</Button>
                </Stack>
                <Stack
                    direction="column"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    spacing={2}
                >   
                    <FormControl>
                        <FormLabel>Username</FormLabel>
                        <Input fullWidth placeholder="Username"></Input>
                    </FormControl>
                    <FormControl>
                        <FormLabel>Password</FormLabel>
                        <Input fullWidth placeholder="Password" type="password"></Input>
                    </FormControl>
                    
                </Stack>
            </Stack>
        </CssVarsProvider>
        
        
    )
}