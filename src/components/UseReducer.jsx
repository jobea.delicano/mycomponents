import { useReducer } from 'react';
import { styles } from '../styles';
import JoyCard from './JoyCard';
import { Button, Sheet, Stack } from '@mui/joy';

function reducer (state, action) {
    switch(action.type) {
        case 'subtract1':
            return state - 1;
        case 'add1':
            return state + 1;
        case 'subtract2':
            return state - 2;
        case 'add2':
            return state + 2;
    }
}

export default function UseReducer () {
    
    const [count, dispatch] = useReducer(reducer, 0);

    function handleSubtract() {
        dispatch({ type: 'subtract1' });
    }

    const definition = [
        'Similar to useState(), causes component re-evaluation when changed',
        'dispatch function sets value of the state depending on the action type',
    ]

    return (
        // <section className="w-full p-8 rounded shadow-md bg-gradient-to-b from-stone-100 to-stone-200">
        //     <h1 className="text-2xl text-center">useReducer()</h1>
        //     <hr></hr>
        //     <ul>
        //         <li>Similar to useState(), causes component re-evaluation when changed</li> 
        //         <li>dispatch function sets value of the state depending on the action type</li> 
        //     </ul>
        //     <br></br>
        //     <section className="flex justify-center ">
        //         <Button className={styles.ButtonClass} onClick={ () => dispatch({type: 'subtract2'}) }> -- </Button>
        //         <Button className={styles.ButtonClass} onClick={handleSubtract}> - </Button>
        //         <label className={styles.labelClass}>{count}</label>
        //         <Button className={styles.ButtonClass} onClick={ () => dispatch({type: 'add1'}) }> + </Button>
        //         <Button className={styles.ButtonClass} onClick={ () => dispatch({type: 'add2'}) }> ++ </Button>
        //     </section>
        // </section>

        <JoyCard title="useReducer()" definition={definition}>
            <Stack direction="row" justifyContent="center" spacing={1}>
                <Button onClick={ () => dispatch({type: 'subtract2'}) }> -- </Button>
                <Button onClick={handleSubtract}> - </Button>
                <Sheet variant="outlined" color="neutral" sx={{ p: 2 }}>{count}</Sheet>
                <Button onClick={ () => dispatch({type: 'add1'}) }> + </Button>
                <Button onClick={ () => dispatch({type: 'add2'}) }> ++ </Button>
            </Stack>
        </JoyCard>
    )
}