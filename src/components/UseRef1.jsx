import { useState, useRef } from 'react';
import { Input, Button, Sheet, Stack } from '@mui/joy';
import JoyCard from './JoyCard';

export default function UseRef1 () {
    const name = useRef('');
    const [output, setOutput] = useState(null);

    function handleSubmit() {
        console.log(name.current.value)
        setOutput(name.current.value);
    }

    const definition = [
        'Can be used to access DOM elements',
        'Will not cause component to refresh when updated',
        'Value won\'t reset when component refreshes unlike normal variable',
        'Important: When using JoyUI, ref prop should be added in slotProps prop for input fields'
    ]
    return (
        <JoyCard title="useRef" definition={definition}>
            <Stack direction="column" justifyContent="center" spacing={1}>
                <Input type="text" slotProps={{ input: { ref: name } }} /> 
                <Button onClick={handleSubmit}>Submit</Button>
                <Sheet variant="outlined" color="neutral" sx={{ p: 2 }}>Hello, {output ?? 'World'}!</Sheet>
            </Stack>
        </JoyCard>
    )
}