import { Card, ListItem, Typography, List, Grid, CardContent } from "@mui/joy";

export default function JoyCard({title, definition, children}) {
    return (

        <Grid xs={4}>
            <Card size="lg" sx={{ minHeight: 360 }}>
                <Typography level="title-lg">{title}</Typography>
                <CardContent orientation="vertical">
                    {definition.map((item)=><Typography level="title-sm">{item}</Typography>)}
                    {children}
                </CardContent>        
            </Card>
        </Grid>

    );
}