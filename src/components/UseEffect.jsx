import { useState, useEffect } from 'react';
import { styles } from '../styles';
import JoyCard from './JoyCard';
import { Button, Sheet, Stack } from '@mui/joy';

function MiniComponent() {
    useEffect(
        ()=> {
            console.log('Start Minicomponent');

            return () => console.log('Goodbye!')
        })

    return (
        <>
            <Sheet>I'm a component!</Sheet>
        </>
    )
}

export default function UseEffect () {
    const [show, setShow] = useState(false);

    function handleShow() {
        setShow((prev)=>!prev);
    }
    
    const definition = [
        'Runs every time the component wherein the useEffect is used is mounted',
        'Note: useEffect may triggers when StrictMode is ON',
    ]
    return (
        // <section className="w-full p-8 rounded shadow-md bg-gradient-to-b from-stone-100 to-stone-200">
        //     <h1 className="text-2xl text-center">useEffect()</h1>
        //     <hr></hr>
        //     <ul>
        //         <li>Runs every time the component wherein the useEffect is used is mounted</li>
        //         <li>Note: useEffect may triggers when StrictMode is ON</li>
        //     </ul>
        //     <br></br>
        //     <section className="flex justify-center ">
        //         <button className={styles.buttonClass} onClick={handleShow}> {show ? 'Hide' : 'Show'} </button>
        //         {show && <MiniComponent/>}
        //     </section>
        // </section>
        <JoyCard title="useEffect" definition={definition}>
            <Stack direction="column" justifyContent="center" spacing={1}>
                <Button onClick={handleShow}> {show ? 'Hide' : 'Show'} </Button>
                {show && <MiniComponent/>}
            </Stack>
        </JoyCard>
    )
}