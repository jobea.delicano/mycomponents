export default function ContentBox ({children, title, ...props}) {
    return (
        <section className="w-full mx-auto max-w-sm p-8 rounded shadow-md bg-gradient-to-b from-stone-100 to-stone-200">
            <h1 className={props.textColor}>{title}</h1>
            <hr></hr>
            {children}
        </section>
    )
}