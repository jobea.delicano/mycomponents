export default function Button({children, type, width, ...props}) {
    let buttonClass = "my-1 px-4 py-2 font-semibold uppercase rounded shadow  "

    if (width === "full") {
      buttonClass += " w-full "
    }

    if (type === "primary") {
      buttonClass+= "text-stone-100 bg-gradient-to-b from-red-800 to-red-900"
    } else {
      buttonClass+= "text-stone-900 bg-gradient-to-b from-slate-100 to-slate-200"
    }
    return (
      <>
        <button className={buttonClass} {...props}>{children}</button>
      </>
    );
  }
  