import { useState } from 'react';
import { styles } from '../styles';
import { Alert, Button, Card, FormLabel, Sheet, Stack, Typography } from '@mui/joy';
import JoyCard from './JoyCard';

export default function UseState () {
    const [count, setCount] = useState(0);
    //      ^        ^
    //    value   setter function

    function handleAdd() {
        setCount((prev)=>prev+1);
    }

    function handleSubtract() {
        setCount((prev)=>prev-1);
    }

    const definition = [
        'Causes component re-evaluation when state is updated.',
        'Setter function is used to update the state'
    ]

    return (
        <JoyCard title="useState()" definition={definition}>
            <Stack direction="row" justifyContent="center" spacing={1}>
                <Button onClick={handleSubtract}> - </Button>
                <Sheet variant="outlined" color="neutral" sx={{ p: 2 }}>{count}</Sheet>
                <Button onClick={handleAdd}> + </Button>
            </Stack>
        </JoyCard>
    )
}