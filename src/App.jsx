import Input from './components/Input.jsx';
import Button from './components/Button.jsx';
import UseState from './components/UseState.jsx';
import UseReducer from './components/UseReducer.jsx';
import UseRef1 from './components/UseRef1.jsx';
import UseRef2 from './components/UseRef2.jsx';
import UseEffect from './components/UseEffect.jsx';
import Joy from './components/Joy.jsx';
import { Stack, Typography, Box, Grid } from '@mui/joy';

function App() { 
  return (
    <>  
      <Joy/>
  
      {/* <header className="m-4 p-4 bg-gradient-to-b rounded-xl from-stone-600 to-stone-900">
        <h1 className="text-3xl text-stone-100 text-center">React JS + Tailwind CSS</h1>
      </header>
      
      <section className="w-full mx-auto max-w-sm p-8 rounded shadow-md bg-gradient-to-b from-stone-100 to-stone-200">
        <Input label="Username" placeholder="Username"/>
        <Input label="Email" placeholder="Email"/>
        <Input label="Password" placeholder="Password" type="password"/>
        <Button type="primary" width="full" onClick="">Login</Button>
        <Button type="default" width="full">Clear</Button>
      </section> */}

      <Box mx={2}>
        <Typography level="h1">Basic React Hooks</Typography>
        <Grid
          container
          rowSpacing={2}
          columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          sx={{ width: '100%' }}
        >
          <UseState/>
          <UseReducer/>
          <UseRef1/>
          <UseRef2/>
          <UseEffect/>
        </Grid>
      </Box>
      
    </>
    
  );
}

export default App;
