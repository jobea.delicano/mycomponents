export const styles = 
    {
        buttonClass: "mr-1 p-4 text-stone-900 bg-gradient-to-b from-slate-300 to-slate-400",
        buttonClassSubmit: "p-1 text-stone-900 bg-gradient-to-b from-red-300 to-red-400",
        labelClass: "mr-1 p-4 text-stone-900 bg-gradient-to-b from-slate-100 to-slate-200 text-center",
        inputClass: "mb-1 w-full px-3 py-2 leading-tight border rounded shadow text-center",
    };