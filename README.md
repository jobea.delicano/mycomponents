# Running this project

1. Clone repo
2. `npm install`
3. `npm run dev`

# React + Joy UI

## Initializing Project Folder
```
npm create vite@latest

Choose project name
Choose package name
Framework: React
Variant: JavaScript

cd ProjectName
npm install
```

## Install JoyUI

From https://mui.com/joy-ui/getting-started/installation/

```
npm install @mui/joy @emotion/react @emotion/styled
npm install @fontsource/inter
```

## Finally, run the app.
```
npm run dev
```

# React + Tailwind own setup guide (Old guide)
dahil makakalimutin ako

## Initializing Project Folder
```
npm create vite@latest

Choose project name
Choose package name
Framework: React
Variant: JavaScript

cd ProjectName
npm install
```

## Install tailwindcss and postcss 

```
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init
```

### tailwind.config.js
In tailwind.config.js, update value of content:
```
  content: ["./index.html", "./src/**/*.{html,js,jsx}"],
```

### postcss.config.js

Check if postcss.config.js is create and has the following content
```
module.exports = {
  plugins: {
    tailwindcss: {},
    autoprefixer: {},
  }
}
```

If postcss config was not created, create the file using named: postcss.config.cjs and place content above on the created config file

### index.css
Locate index.css and replace content with below text
```
@tailwind base;
@tailwind components;
@tailwind utilities;
```

## Finally, run the app.
```
npm run dev
```